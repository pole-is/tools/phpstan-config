[![Latest Stable Version](https://poser.pugx.org/irstea/phpstan-config/v/stable)](https://packagist.org/packages/irstea/phpstan-config)

# irstea/phpstan-config

Configuration pour phpstan/phpstan.

Inclut les extensions PHPStan pour les outils suivant :

- Doctrine => [phpstan/phpstan-doctrine](https://github.com/phpstan/phpstan-doctrine)
- Symfony => [phpstan/phpstan-symfony](https://github.com/phpstan/phpstan-symfony)
- PHPUnit => [phpstan/phpstan-phpunit](https://github.com/phpstan/phpstan-phpunit)
- [beberlei/assert](https://github.com/beberlei/assert) => [phpstan/phpstan-beberlei-assert](https://github.com/phpstan/phpstan-beberlei-assert)
- [phpspec/prophecy](https://github.com/phpspec/prophecy) => [jangregor/phpstan-prophecy](https://github.com/Jan0707/phpstan-prophecy)

### Installation

```shell
composer require --dev irstea/phpstan-config
```

### Utilisation

Le paquet fournit plusieurs fichiers de configuration à inclure selon les besoins:

- [`loose.neon`](loose.neon): vérifications _souples_,
- [`strict.neon`](strict.neon): vérifications _strictes_ (inclut `loose.neon`),
- [`phpunit.neon`](phpunit.neon): à inclure dans les projets utilisant PHPUnit,
- [`symfony.neon`](symfony.neon): à inclure dans les projets utilisant Symfony et Doctrine.
- [`phpstan.neon`](phpstan.neon): `strict.neon` + `phpunit.neon` + `symfony.neon`.

Exemple :

```neon
includes:
    - vendor/irstea/phpstan-config/phpstan.neon

parameters:
    paths:
        - src
        - tests

    symfony:

        container_xml_path: %currentWorkingDirectory%/var/cache/test/srcApp_KernelTestDebugContainer.xml
```

**Note :** `container_xml_path` change selon la version de Symfony, cf. https://github.com/phpstan/phpstan-symfony#usage
